package com.itis.websocketshwrobertmukhtarov.repository;

import com.itis.websocketshwrobertmukhtarov.model.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChatMessageRepository extends JpaRepository<ChatMessage, Integer> {
    List<ChatMessage> findChatMessagesByChatRoom_IdOrderByDateAsc(int chatroomId);
}
