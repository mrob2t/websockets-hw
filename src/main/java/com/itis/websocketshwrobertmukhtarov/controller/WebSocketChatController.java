package com.itis.websocketshwrobertmukhtarov.controller;

import com.itis.websocketshwrobertmukhtarov.model.ChatMessage;
import com.itis.websocketshwrobertmukhtarov.model.ChatRoom;
import com.itis.websocketshwrobertmukhtarov.service.ChatMessageService;
import com.itis.websocketshwrobertmukhtarov.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class WebSocketChatController {

    @Autowired
    private ChatService chatService;

    @Autowired
    private ChatMessageService messageService;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/chat")
    public void chat(@Payload ChatMessage message) {
        ChatRoom chat = chatService.getOrCreate(message.getSenderId(), message.getRecipientId());
        message.setChatRoom(chat);
        messageService.save(message);
        messagingTemplate.convertAndSendToUser(message.getSenderId(),"/queue/messages", message);
        messagingTemplate.convertAndSendToUser(message.getRecipientId(),"/queue/messages", message);
    }

    @MessageMapping("/chatHistory")
    public void chatHistory(@Payload ChatMessage message) {
        ChatRoom chat = chatService.getOrCreate(message.getSenderId(), message.getRecipientId());
        List<ChatMessage> chatMessages = messageService.getMessagesOfChatroom(chat.getId());
        for (ChatMessage chatMessage : chatMessages) {
            messagingTemplate.convertAndSendToUser(message.getSenderId(),"/queue/messages", chatMessage);
        }
    }
}
