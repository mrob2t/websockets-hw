package com.itis.websocketshwrobertmukhtarov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsocketsHwRobertmukhtarovApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebsocketsHwRobertmukhtarovApplication.class, args);
    }

}
