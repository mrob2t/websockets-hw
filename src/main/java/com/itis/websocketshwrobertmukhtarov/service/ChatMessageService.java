package com.itis.websocketshwrobertmukhtarov.service;

import com.itis.websocketshwrobertmukhtarov.model.ChatMessage;
import com.itis.websocketshwrobertmukhtarov.repository.ChatMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatMessageService {
    @Autowired
    private ChatMessageRepository messageRepository;

    public ChatMessage save(ChatMessage message) {
        return messageRepository.save(message);
    }

    public List<ChatMessage> getMessagesOfChatroom(int chatroomId) {
        return messageRepository.findChatMessagesByChatRoom_IdOrderByDateAsc(chatroomId);
    }
}
