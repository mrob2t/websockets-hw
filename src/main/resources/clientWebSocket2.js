let stompClient;
const currentUserId = '2';
const recipientId = '1';

function send() {
    const textArea = document.getElementById("message")
    const msg = textArea.value
    sendMessage(msg);
    textArea.value = ""
}

function connect() {
    SockJS = new SockJS("http://localhost:8080/ws");
    stompClient = Stomp.over(SockJS);
    stompClient.connect({}, onConnected, onError);
}

function onError() {
    console.log("Error during connection.")
}

function onConnected() {
    document.getElementById("connectionDiv").setAttribute("style", 'visibility: hidden;')
    document.getElementById("chatDiv").setAttribute("style", 'visibility: visible;')
    stompClient.subscribe(
        "/user/" + currentUserId + "/queue/messages",
        received
    );
    getHistory()
}

function received(jsonMessage) {
    let displayedMessage = ""
    const message = JSON.parse(jsonMessage.body);
    displayedMessage += message.senderId === currentUserId ? "<b>Вы:</b> " : "<b>Собеседник:</b> ";
    displayedMessage += message.message;

    const date = new Date(message.date)
    displayedMessage += ` <i>(${date.toLocaleTimeString()} ${date.toLocaleDateString()})</i>`;

    const messagesList = document.getElementById("messagesFromUsers");
    const li = document.createElement("li");
    li.innerHTML += displayedMessage
    messagesList.appendChild(li)
}

const sendMessage = (msg) => {
    if (msg.trim() !== "") {
        const message = {
            senderId: currentUserId,
            recipientId: recipientId,
            message: msg,
            date: new Date(),
        };
        stompClient.send("/webSocketApp/chat", {}, JSON.stringify(message));
    }
};

function getHistory() {
    const message = {
        senderId: currentUserId,
        recipientId: recipientId,
    };
    stompClient.send("/webSocketApp/chatHistory", {}, JSON.stringify(message));
}
